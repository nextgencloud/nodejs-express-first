# express 프로젝트 

### 목차
1. 프로그램 설치
   1. ide 환경 설정
      - extend 소개 
   2. node & npm 설치
        - node 설치
        - npm 설치
2. nodejs 프로젝트 생성
   1. 디렉토리 생성
   2. npm

3. git 연동
   1. .gitignore 파일 생성
   2. gitlab project 생성
   3. push a local project to a remote repository

4. node js 실행하기


## 1. 프로그램 설치
### vscode 설치
  
  https://code.visualstudio.com 에서 로컬 개발환경에 적합한 버전으로 프로그램을 다운로드 하고 설치한다.

### extends 설치

nodejs를 개발하기 위해 유용한 extends를 받아서 설치한다.
- marketplace에서 검색하거나 ide의 extentions(Ctr+shift+X) 메뉴에서 검색하여 설치한다.
  - Expressjs Snippets
  - VS Code JavaScript (ES6) 
  - Node.js Extension Pack
  - Node.js Modules Intellisense
  - npm install -g jshint (code template 자동생성)
  - Markdown Support for Visual Studio Code

  

### node & npm 설치

javascript는 브라우저에서 실행하는 스크립트 언어이다. 이때 window라는 최상위 객체 하위에 모든 객체들이 존재하며 브라우저에서 코드를 실행할 수 있다. nodejs의 경우 별도의 실행 파일이 존재하여 브라우저가 아닌 실행 파일로 javascript를 구동할 수 있다. 이때 사용하는 런타임 실행환경이 node.exe 이다.

https://nodejs.org/ko/ 사이트에 가면 2종류의 런타임을 제공하는데 lts 최신 버전으로 설치한다.


## 2. nodejs 프로젝트 시작

### 최초 프로젝트 시작하기
1. 작업 폴더 생성
   
   프로젝트를 생성하기 위해 디렉토리를 만든다. 디렉토리 생성 후 생성된 디렉토리로 이동한다. code . 를 호출하여 vscode를 실행한다.

2. npm 명령실행
   
    **npm init** 명령으로 프로젝트 초기 정보를 입력한다.
    package.json 파일이 생성된다.
    
3. express 초기화
   
    **npm install express --save** 명령을 실행한다. package-lock.json 파일이 생성되고 하위에 node_modules 폴더가 생성된다.


## 3. git 연동하기

1. .gitignore 파일 생성

    gitignore 파일로 불피요한 파일을 커밋하지 않는다.
    https://www.gitignore.io 에 접속하여 환경에 맞는 정보를 선택하면 파일이 생성된다.
    이 정보를 선택 및 복사하여 프로젝트 루트에 .gitignore 파일을 생성한 후 붙여넣기를 하면 된다.

2. gitlab project 생성

    gitlab 프로젝트를 생성한다. 이때 **Initialize repository with a README** 파일 생성에 체크하지 않고 프로젝트를 생성한다.

3. push a local project to remote repository
    
    **git init** 명령으로 프로젝트를 초기화 한다.
    
    **git remote add origin https://gitlab/user/project_name.git** 명령으로 리모트 저장소를 로컬 저장소에 연결한다.

    **git remote -v** 로 연결된 정보를 확이한다.

    **git add .** 명령으로 로컬 저장소 index에 추가한다.
    
    **git commit -m 'initail commit'** 명령으로 로컬 저장소에 커밋 한다.
    
    **git push orign master** 명령으로 로컬 저장소의 파일을 원격 저장소 서버에 푸시한다.

## 4. node js 실행하기
프로젝트 루트에서 **node index.js** 명령을 실행한다. 콘솔에 Server started on por 로그가 찍히면서 서버가 실행된다.
브라우저에서 **http://localhost:3000/** 로 접속하면 test 라는 문구가 브라우저에 출력된다. 종료는 **ctl+B** 로 중지한다.


## 5. git  clone 후 프로젝트 초기화
git에서 복사후 최초 한번 프로젝트 초기화를 수행한다.
```bash
npm install express --save
```


## git commit error 처리
git push시 권한 에러 발생할 수 있음
```bash
git push origin master
remote: You are not allowed to push code to this project.
fatal: unable to access 'https://gitlab.com/nextgencloud/nodejs-express-first.git/': The requested URL returned error: 403
```
다음과 같이 remote 정보를 변경하여 권한 에러를 해결한다.
```bash
git remote set-url origin https://user-gitlab-id@gitlab.com/nextgencloud/nodejs-express-first.git   // url 정보 변경
git remote -v // 확인

origin	https://user-gitlab-id@gitlab.com/nextgencloud/nodejs-express-first.git (fetch)
origin	https://user-gitlab-id@gitlab.com/nextgencloud/nodejs-express-first.git (push)
```
위와 같이 주소 정보가 바뀌면 정상적으로 push가 수행된다.
```bash
git push origin master

Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 424 bytes | 424.00 KiB/s, done.
Total 4 (delta 3), reused 0 (delta 0)
To https://gitlab.com/nextgencloud/nodejs-express-first.git
   a51aef0..8ad0933  master -> master
```


